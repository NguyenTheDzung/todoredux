import * as types from './actionTypes';
import {v4 as uuidv4} from 'uuid';
import {createTasks} from './../common/index'

const initiaState = {
    todos: []
};
const todosReducer = (state = initiaState, action) => {
    switch (action.type) {
        case types.ADD_TODO:
        const addedTodo = [...state.todos, action.payload];
        return {
            ...state,
            todos: addedTodo,
        };
        case types.REMOVE_TODO:
            const filterTodo = state.todos.filter((t) => t.id !== action.payload.id);
            return {
                ...state,
                todos: filterTodo,
            }
        case types.COMPLETE_TODO:
            const toggleTodo = state.todos.map((t) => 
            t.id === action.payload.id ? {...action.payload, completed: !action.payload.completed} : t);
            return {
                ...state,
                todos: toggleTodo,
            };
            default:
                return state;
    }
}

export default todosReducer